package lafourmi;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


public class LaFourmiDeLangton extends JFrame{
	private Container panneau;
	private JButton monBouton;
	private JLabel monLabel;
	private int dimension;
	
	public LaFourmiDeLangton(){
		super("La Fourmi de Langton");
		setSize(500, 500);
		setLocation(300,200);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panneau = getContentPane();
		
		monBouton = new JButton("Next");
		monLabel = new JLabel("Nombre de tours = 0");
		
		panneau.add(monLabel,BorderLayout.NORTH);
		panneau.add(monBouton,BorderLayout.SOUTH);
		
		
		setVisible(true);
	}
	public static void main(String[] args) {
		LaFourmiDeLangton n = new LaFourmiDeLangton();
	}
}